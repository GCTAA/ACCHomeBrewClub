# Why do we "boot" computers?

By Kevin Cole

There is a humorous idiom about the way to climb a fence is to "pull oneself up
by one's own bootstraps".

Some boots as you may have guessed or experienced, have straps designed to help
you pull them onto your feet and legs. The joke is that if you could pull them
even harder, you could "levitate".

When you start a computer, even as old as the Altair, there is a small section
of memory that is permanently set -- usually by the manufacturer. It is
typically something that owners and users (and programmers) cannot easily
change. It is Read Only Memory (ROM). It cannot be written to.

Part of the ROM has a small program that looks for a hard disk, and if it finds
a hard disk, it looks in a very specific location on the hard disk. Just as you
have already learned with memory, there are numeric addresses that reference
specific locations. On the original hard disks, you had several **platters**
(circular plates, like in an old record player) that were coated with a
magnetic paint. An electromagnet could magnetize (set to 1) or demagnetize (set
to 0) very small areas of the disk, *writing* to it. Another part of the system
could detect the ones and the zeros, thus *reading* from the disk.

Data was stored in concentric rings on both the top and bottom of each platter.
So, just as we have latitude, longitude and altitude, or X, Y and Z to describe
a specific in the physical 3D world, so too does a hard disk have 3 dimensions:
cylinder, sector and head.

* **cylinder** refers to which concentric ring of a platter
* **sector** refers to a pie slice of a platter
* **head** refers to which platter, and which side (top or bottom) of the
  platter

A picture is worth a thousand words so,

![Hard disk geometry](https://upload.wikimedia.org/wikipedia/commons/thumb/0/02/Cylinder_Head_Sector.svg/360px-Cylinder_Head_Sector.svg.png)

But. back to the question: When you first turn the computer on, there is no
operating system. There is no concept of what a file, or a filename is, but
just as you have seen with machine language, there is the concept of *locations
referenced by a numeric address and data and machine language instructions at
those locations*.

The ROM says "Look at a specific cylinder, sector and head. There you will find
-- I don't know, let's say 200 -- bytes that are machine instructions. Copy
those 200 bytes to RAM, putting the first byte at, oh, let's say memory
location 500 octal. Finally jump (JMP) to memory location 500 octal."

OK, the ROM has handed off running the computer to the RAM. Well, the program
at 500 octal says "Look at this other location on the hard disk, and load 3000
bytes into memory location 10000 octal, and jump there." Well, this is now a
much bigger program than the original 40-byte program in ROM, or the 200-byte
program that it loaded into RAM originally. Bigger, better, stronger, faster...
Well maybe not faster. But it is more complex, and able to do more.

Slowly, the computer is building its operating system by having little programs
load bigger programs which load bigger programs, every time you start the
machine.

Some programmers that considered themselves to be brilliant comedians said that
the computer was pulling itself up by its own bootstraps.

This initial ROM program to do this became known as a **bootstrap loader** and
the process eventually just started to be called **booting**.

"Ain't that a kick in the pants?", says one of those programmers [me] who
considers himself a brilliant comedian and is sadly mistaken. Get it? "Boot"?
"Kick"? Never mind.

But, on a related note, why is there a crowd-funding site named
kickstarter.com? There's a similar origin: Many motorcycle engines are started
by pushing a lever with one's foot, thus *kick starting* it. (If you kick start
something while pulling yourself up by your bootstraps, I think you'll fall
flat on your face.)
