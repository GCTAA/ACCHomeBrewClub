# Which Endian?

By Kevin Cole

So... Riddle me this, Batmen / Batwomen: Is the Altair 8800 a big-endian or
little-endian architecture? What, you might well ask, does that even mean??
Read on....

We've already abstracted away the movement of electrons through a wire or lack
thereof to mean 1 or 0.

We've further assigned a human concept of **sequential order** to these
abstract notions called bits and bytes... but they're sort of, potentially,
contradictory in a way:

Think about this: We organize our thinking about computers with a "Western"
bias towards "Western" written languages. We read left to right, and top to
bottom.

So, when we're talking about computer memory, or looking at the pull-down tab
in the web-based emulator, it is organized as:

    Byte 0, Byte 1, Byte 2, Byte 3,
    Byte 4, Byte 5, Byte 6, Byte 7,

and so on. Left to right, top to bottom.

But, when we think about the bits within the byte, we think of them backwards
-- right to left -- because that's how we organize magnitude of numbers (2 to
the power of):

    Bit 7, Bit 6, Bit 5, Bit 4, Bit 3, Bit 2, Bit 1, Bit 0

The least significant bit, with the lowest magnitude -- i.e. the "smallest
number" goes to the far right and values get "bigger" going to the left.

Both of those are completely abstract. Those bits and bytes exist as very small
transistors on a chip that is smaller than a penny, and are layered in three
dimensions, not the two-dimensional system known as "text". (I suppose you
could argue that a book also has a front-to-back order, giving the third
dimension, but let's not quibble.)

So. The riddle above is asking "What happens when you have an integer that
exceeds 8 bits? How do you represent it? Bit order or byte order?" In other
words, should it be like:

    15 14 13 12 11 10 09 08       07 06 05 04 03 02 01 00
                     Byte 0                        Byte 1

(Bit order "makes sense" as the powers of two correspond with our sense of
numeric magnitude) OR:

    07 06 05 04 03 02 01 00        15 14 13 12 11 10 09 08
                     Byte 0                         Byte 1

(Byte order "makes sense" because the "low order" byte with the lesser
magnitude a.k.a. least significant bits comes at a "lower" memory address than
the "high order" byte with the most significant bits.)

The answer to the riddle is "It depends on who made the machine". And the
difference between the two systems of organization is known by the silly word
"Endianness". An architecture can be **Big-Endian (BE)** or
**Little-Endian (LE)**.

I refer you to the font of all knowledge,
[Wikipedia](https://en.wikipedia.org/wiki/Endianness), for a deeper discussion.

NORMALLY, you won't care. The programming language abstracts the details away
from you. HOWEVER, when it comes to a challenge such as "Implement an adder
that can handle 24-bit numbers on an 8-bit architecture", it becomes
significantly (pun intended) more important. 
