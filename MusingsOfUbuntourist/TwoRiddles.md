# Two Program Riddles

By Kevin Cole

The question for each is what is the end result after running them? In other
words, which memory locations are changed, and what are the new values in those
locations?

The programs are fairly small, and machine instructions in both are limited to
instructions you have all been working with since the first *toggling in*.

I really think it would be a disservice to you to give you any hints on these.
I would also strongly suggest NOT giving your classmates answers if you figure
it out first. It's not meant to be torture, but I'm using simple instructions
in non-obvious ways.

My hope is that you will have what I personally refer to as a "Helen Keller
moment", of which I've had several: It's that moment where, after someone
repeats something to you for the millionth time, or you try something for the
millionth time -- like finger-spelling "W-A-T-E-R" into your hand again and
again, you suddenly shout "WATER!!!! SHOW ME MORE!!!" (Funny thing about water
and moments like that: Archimedes had a similar Eureka! moment and his involved
water too...)

These are meant to be a little tricky. But there's nothing new. I think, if you
understood the addition program that you toggled in, that you should be able to
figure which locations will be changed. The goal is to be able to use what you
have learned to trace the execution of these programs.

Try to figure out WHY they change, taking them step-by-step on a piece of
paper. Look at each instruction alone. Don't try to do the whole program at
once. Make columns on the paper for each register or memory location that is
used, and look at the first instruction. Write down what it changes. Is it an
LDA instruction? Then it changed the value in the accumulator (the column
labeled "A"). What did it change the value of the accumulator to? Write it in
the column. Next line: Is it an STA? Then it's storing the accumulator's value
somewhere. Where? There should be a column with the memory address of where it
is storing the value. What's being stored there? The value that is in the
accumulator, which should be in the "A" column on the previous row. Write that
value in the column for the memory location.

## Riddle 1:

    STEP  MNEMONIC     BIT PATTERN  OCTAL EQUIVALENT
    ----  -----------  -----------  ----------------
     0    LDA          00 111 010     072
     1    (address)    00 000 000     000
     2    (address)    00 000 000     000
     3    STA          00 110 010     062
     4    (address)    10 000 000     200
     5    (address)    00 000 000     000
     6    LDA          00 111 010     072
     7    (address)    00 000 001     001
     8    (address)    00 000 000     000
     9    STA          00 110 010     062
    10    (address)    10 000 001     201
    11    (address)    00 000 000     000
    12    LDA          00 111 010     072
    13    (address)    00 000 010     002
    14    (address)    00 000 000     000
    15    STA          00 110 010     062
    16    (address)    10 000 010     202
    17    (address)    00 000 000     000
    18    JMP          11 000 011     303
    19    (address)    00 000 000     000
    20    (address)    00 000 000     000

## Riddle 2:

    STEP  MNEMONIC     BIT PATTERN  OCTAL EQUIVALENT
    ----  -----------  -----------  ----------------
     0    LDA          00 111 010     072
     1    (address)    01 000 011     103
     2    (address)    00 000 000     000
     3    STA          00 110 010     062
     4    (address)    00 001 101     015
     5    (address)    00 000 000     000
     6    LDA          00 111 010     072
     7    (address)    01 000 000     100
     8    (address)    00 000 000     000
     9    MOV (A-&gt;B)   01 000 111     107
    10    LDA          00 111 010     072
    11    (address)    01 000 001     101
    12    (address)    00 000 000     000
    13    ADD (A+B)    10 000 000     200
    14    STA          00 110 010     062
    15    (address)    01 000 010     102
    16    (address)    00 000 000     000
    17    JMP          11 000 011     303
    18    (address)    00 000 000     000
    19    (address)    00 000 000     000

And memory locations 100 octal to 103 octal contain the data: 

    BIT PATTERN  OCTAL EQUIVALENT
    -----------  ----------------
    00 000 101     005
    00 001 000     010
    00 000 000     000
    10 010 000     220

## Resource:

* [Appendix A: Altair 8800 Instruction Set](https://ubuntourist.codeberg.page/Altair-8800/appendix.html)
