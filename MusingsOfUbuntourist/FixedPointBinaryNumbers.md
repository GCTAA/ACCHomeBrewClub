# Fixed Point Binary Numbers

by Kevin Cole

Ponder this, padawans:

Given what you know about numbers like 3.14159, and $2.50, and 0.333, meditate
on the binary (a.k.a. base 2) number:

101.11

and consider what its value is in decimal (a.k.a. base 10).

You don't need a computer, and, if binary is becoming your friend, you
shouldn't even need a piece of paper.

You may need time and patience while thinking about the nature of the decimal
number system you are already familiar with, and what is happening with numbers
to the right of the decimal point, applying the same logic to numbers to the
right of the "binary point".

(Fixed point numbers are useful, but floating point numbers are more useful.
However, it's easier to understand fixed point first, and once you have a
handle on that concept, floating point shouldn't be a big leap... I hope.)

Hint: Think about numbers represented in scientific notation...
