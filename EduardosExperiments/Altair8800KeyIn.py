# to prevent the programm from going to fast
from sys import intern
from time import sleep
import math

# for the pynput library
from pynput.mouse import Button, Controller

# for the exel file
import openpyxl

# for the screen resolution
import pyautogui

# variables

mouse = Controller()

# get the screen resolution
screenWidth, screenHeight = pyautogui.size()

scaleUp = screenWidth/16

# get the values of a specific colum in the exel as input

wb_obj = openpyxl.load_workbook(filename='Altair8800_Programs-Modified_ClearRAM.xlsx')

sheet_obj = wb_obj.active

getValues = []

for c in sheet_obj['C']:
    getValues.append(c.value)


print(getValues)

# format is (x, y) pixels for a 16:9 screen (later going to be scaled up)
dataInSwitches = {
    '0':1610,'1':1555, '2':1485,        # this is the x position for the data switches. There is no y position because the switches are on the same y axis
    '3':1390,'4':1325, '5':1260,
    '6':1160, '7':1095,
    }

operationSwitchesX = {
    'DEPOSIT':860, 
    'DEPOSIT NEXT': 860,            # this is the x and y positions for the operation/instruction switches. Unlike the data switches, their y and x positions are different
    'RESET': 995,
    'EXAMINE': 725,
    }

operationSwitchesY = {
    'DEPOSIT':595, 
    'DEPOSIT NEXT': 675, 
    'RESET': 590,
    'EXAMINE': 595,
}

# Function to flip the instruction switches, grabs data from the operation swiches x and y dictionarys. This is not a complete list

def returnSwitch(instruction):
    if(instruction == 'DEPOSIT'):
        mouse.position = ((operationSwitchesX['DEPOSIT']/120)*scaleUp, (operationSwitchesY['DEPOSIT']/120)*scaleUp)
        sleep(0.06)
        mouse.click(Button.left, 1)
        sleep(0.06)

    elif(instruction == 'DEPOSIT NEXT'):
        mouse.position = ((operationSwitchesX['DEPOSIT NEXT']/120)*scaleUp, (operationSwitchesY['DEPOSIT NEXT']/120)*scaleUp)
        sleep(0.06)
        mouse.click(Button.left, 1)
        sleep(0.06)

    elif(instruction == 'RESET'):
        mouse.position = ((operationSwitchesX['RESET']/120)*scaleUp, (operationSwitchesY['RESET']/120)*scaleUp)
        sleep(0.06)
        mouse.click(Button.left, 1)
        sleep(0.06)

    elif(instruction == 'EXAMINE'):
        mouse.position = ((operationSwitchesX['EXAMINE']/120)*scaleUp, (operationSwitchesY['EXAMINE']/120)*scaleUp)
        sleep(0.06)
        mouse.click(Button.left, 1)
        sleep(0.06)

    elif(instruction == 'None'):
        print('no deposit, deposit next or reset instructions')

# This function flips the data switches if the number inputed is a 1 or a zero. Re-runing this function will un-flip the data switches reseting them to their original position

def inputDataSwitch(instruction):
    counter = 7                     # used to search the x coordinate in the dataSwitches dictionary
    for number in instruction:
        if(number == '1'):
            mouse.position = ((dataInSwitches[str(counter)]/120)*scaleUp, 4.125*scaleUp)
            sleep(0.06)
            mouse.click(Button.left, 1)     # click on the data switch
            sleep(0.06)
            counter -= 1
        elif(number == '0'):
             counter -= 1       # skip to the next number
        else:
            counter = counter   # used to handle white spaces or other characters

# the proggram beggins

# make the hex numbers into binary
for index, element in enumerate(getValues):
    if index % 2 == 0:
        if(element != None):
            element2 = str(element)
            element2 = element2.capitalize()
            getValues.remove(element)
            integer = int(element2, 16)
            length = 8
            spec = '{fill}{align}{width}{type}'.format(fill='0', align='>', width=8, type='b')
            res = format(integer, spec)
            getValues.insert(index, str(res))

print(getValues)

for index, elem in enumerate(getValues):
    if (index + 1 < len(getValues) and index - 1 >= 0):    # get the next and current element from getValues
        curr_el = str(elem)
        next_el = str(getValues[index + 1])
        if(curr_el != 'DEPOSIT' or curr_el != 'DEPOSIT NEXT' or curr_el != 'RESET' or curr_el != 'EXAMINE'):    # check if we are in a binary number and if we are then we input that binary number
            print('we are in a binary')
            inputDataSwitch(curr_el)
            if(next_el == 'DEPOSIT' or next_el == 'DEPOSIT NEXT' or next_el == 'RESET'  or next_el != 'EXAMINE'):
                print('the next elemet is a instruction, lets execute it ')                    
                returnSwitch(next_el)                                                     # check if the next element is a instruction, execute that instruction and reset the data switches back to their original state
                print('its been executed, now lets reset the data swiches')
                inputDataSwitch(curr_el)