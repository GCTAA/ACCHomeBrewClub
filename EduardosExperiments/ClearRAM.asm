;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;   
;NAME: Clear the RAM
;AUTHOR: Eduardo
;LASTMOD: 11.10.2021
;
;DESCRIPTION:
;
;   This is a program that will set all the addresses in the Altair8800 to 0
;   essentially clearing the RAM from all the "garbage" it has and replace it
;   with nice NOP (no operation) instructions. It can now be used to clear a
;   specific amount of address (max 255) and start at any address you choose.
;   It can be used as a subroutine to clear the RAM. 
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

        ORG     00ah                ; Load at memory location 16

CLEAR:  LXI     D,VAL1              ; Load the register pair DE with the address where we will start replacing the "garbage"
        LXI     B,VAL2              ; The amount of addresses that will be replaced has been loaded to register pair BC
                                    ; VAL3 can't be bigger than 255 because we only use register C
        LDA     009h                ; Load the accumulator with the value at address 9
HERE:   STAX    D                   ; Store the accumulator contents in the address specified by register pair DE
                                    ; When register C becomes 0, the zero status bit changes so we can call this instruction 
        JMP     HERE                ; Loop the program

        ORG     000h                ; Load at memory location 20 right after the SUBROUTINE ends

START:  CALL    CLEAR               ; Initiate SUBROUTINE
LOOP:   NOP                         ; This will be our MAIN event loop. From here we can call SUBROUTINE as many times as we want
        JMP     LOOP                ; Loop the program at the last address so that we don't loop all the way around

;   Data Segment:
        ORG     007h        ; Load at memory location 7

VAL1:   DB      01fh        ; Data Byte at address 7 = 31
                            ; This is the value where we want to start clearing the RAM
VAL2:   DB      0c8h        ; Data Byte at address 8 = 200
                            ; VAL2 is how many spaces do we want to clear or set to a NOP operation
VAL3:   DB      000h        ; Data Byte at address 9 = 0
                            ; VAL3 is what we will replace the RAM addresses with (in this case NOP)

        END

;Here it is in hex:
;1b 00 0f 00 11 1b 00 01 0f 00 3a 03 00 12 13 0d
;c8 c3 0d 00 cd 04 00 00 c3 17 00