#  ACC Home Brew Computer Club

This respository contains programs, resources, and musings related to the
fun and learning students and friends of the
[Arlington Career Center](https://careercenter.apsva.us/) have been doing
on their [Altair 8800 clone](https://altairclone.com/) computer.

## Musings of Ubuntourist

* [Two Program Riddles](https://codeberg.org/GCTAA/ACCHomeBrewClub/src/branch/main/MusingsOfUbuntourist/TwoRiddles.md)
* [Fixed Point Binary Numbers](https://codeberg.org/GCTAA/ACCHomeBrewClub/src/branch/main/MusingsOfUbuntourist/FixedPointBinaryNumbers.md)
* [Why Do We Boot Computers?](https://codeberg.org/GCTAA/ACCHomeBrewClub/src/branch/main/MusingsOfUbuntourist/WhyDoWeBootComputers.md)
* [Which Endian?](https://codeberg.org/GCTAA/ACCHomeBrewClub/src/branch/main/MusingsOfUbuntourist/WhichEndian.md)
